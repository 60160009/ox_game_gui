
public class Board {
	private char table[][] = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
	private Player X;
	private Player O;
	private Player winner;
	private int turnCount;
	private Player current;
	private String welcome;

	public Board(Player X, Player O) {
		this.X = X;
		this.O = O;
		winner = null;
		turnCount = 0;
		current = X;
		welcome = "Start Game OX ";
	}

	public boolean isFinish() {
		if(checkWin() == true) {
			if(winner.getName() == 'X') {
				X.addWin();
				O.addLose();
			}else {
				O.addWin();
				X.addLose();
			}
			return true ;
		}else  if (checkDraw() == true) {
			X.addDraw();
			O.addDraw();
			return true ;
		}
		return false ;
	}

	public char[][] getTable() {
		return table;
	}

	public Player getCurrent() {
		return current;
	}

	public boolean setTable(String row, String col) {

		try {
			int R = Integer.parseInt(row);
			int C = Integer.parseInt(col);

			if (R > 3 || R < 0 || R == 0 || C > 3 || C < 0 || C == 0) {
				System.out.println("Row and Column must be number 1-3");
				return false ;
			}
			R = R - 1;
			C = C - 1;

			if (table[R][C] != ' ') {
				System.out.println("Row " + (R + 1) + " and Column " + (C + 1) + " can't choose again");
				return false  ;
			}
			turnCount++;
			table[R][C] = current.getName();
			return true ;

		} catch (Exception a) {
			System.out.println("Row and Column must be number");
			return false ;
		}
	}
	

	public void switchTurn() {
		if (current.getName() == 'X') {
			current = O ;
		} else {
			current = X;
		}
	}

	public String getWelcome() {
		return welcome;
	}

	public Player getWinner() {
		return winner;
	}
	
	
	
	public boolean checkWin() {
		boolean chk = false;
		// ���ǹ͹
		for (int i = 0; i < table.length; i++) {
			if (table [i][0] == current.getName() && table[i][1] == current.getName() && table[i][2] == current.getName()) {
				chk = true;
			}
		}
		// ���ǵ��
		for (int i = 0; i < table.length; i++) {
			if (table[0][i] == current.getName() && table[1][i] == current.getName() && table[2][i] == current.getName()) {
				chk = true;
			}
		}
		// �����§
		if (table[0][0] == current.getName() && table[1][1] == current.getName() && table[2][2] == current.getName()) {
			chk = true;
		}
		if (table[0][2] == current.getName() && table[1][1] == current.getName() && table[2][0] == current.getName()) {
			chk = true;
		}
		if (chk == true) {
			winner = current ;
			return true ;
		}
		return false ;
	}
	
	public boolean checkDraw() {
		if (turnCount == 9) {
			winner = null ;
			return true ;
		}
		return false ;
	}
	
	public void clearBoard() {
		turnCount = 0 ;
		current = X ;
    	for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				table[i][j] = ' ' ;
			}
		}
	}

}
